"""OpenVidu class."""
from typing import List, Union
from functools import partial

from requests_toolbelt.sessions import BaseUrlSession
from requests.auth import HTTPBasicAuth
from requests_toolbelt import user_agent

from . import __version__
from .exceptions import OpenViduSessionDoesNotExistsError, OpenViduSessionExistsError
from .openvidusession import OpenViduSession


class OpenVidu(object):
    """
    This object represents a OpenVidu server instance.
    """

    def __init__(self, url: str, secret: str, initial_fetch: bool = True, timeout: Union[int, tuple, None] = None):
        """
        :param url: The url to reach your OpenVidu Server instance. Typically something like https://localhost:4443/
        :param secret: Secret for your OpenVidu Server
        :param initial_fetch: Enable the initial fetching on object creation. Defaults to `True`. If set to `False` a `fetc()` must be called before doing anything with the object. In most scenarios you won't need to change this.
        :param timeout: Set timeout to all Requests to the OpenVidu server. Default: None = No timeout. See https://2.python-requests.org/en/latest/user/advanced/#timeouts for possible values.
        """
        self._session = BaseUrlSession(base_url=url)
        self._session.auth = HTTPBasicAuth('OPENVIDUAPP', secret)

        self._session.headers.update({
            'User-Agent': user_agent('PyOpenVidu', __version__)
        })

        self._session.request = partial(self._session.request, timeout=timeout)

        self._openvidu_sessions = {}  # id:object

        self._last_fetch_result = {}  # Used only to calculate the return value of the fetch() call
        if initial_fetch:
            self.fetch()  # initial fetch

    def fetch(self) -> bool:
        """
        Updates every property of every active Session with the current status they have in OpenVidu Server. After calling this method you can access the updated list of active sessions trough the `sessions` property.

        :return: true if the Session status has changed with respect to the server, false if not. This applies to any property or sub-property of the object.
        """

        r = self._session.get("sessions")
        r.raise_for_status()
        new_data = r.json()['content']

        data_changed = new_data != self._last_fetch_result
        self._last_fetch_result = new_data

        if data_changed:
            self._openvidu_sessions = {}

            # update, create valid streams
            for session_data in new_data:
                session_id = session_data['id']
                self._openvidu_sessions[session_id] = OpenViduSession(self._session, session_data)

        return data_changed

    def get_session(self, session_id: str) -> OpenViduSession:
        """
        Get a currently active session to the server.

        :param session_id: The ID of the session to acquire.
        :return: An OpenViduSession object.
        """
        if session_id not in self._openvidu_sessions:
            raise OpenViduSessionDoesNotExistsError()

        session = self._openvidu_sessions[session_id]

        if not session.is_valid:
            raise OpenViduSessionDoesNotExistsError()

        return session

    def create_session(self, custom_session_id: str = None, media_mode: str = None, recording_mode: str = "MANUAL") -> OpenViduSession:
        """
        Creates a new OpenVidu session.

        This method calls fetch() automatically since the server does not return the proper data to construct the OpenViduSession object.

        https://docs.openvidu.io/en/2.16.0/reference-docs/REST-API/#post-openviduapisessions

        :param custom_session_id: You can fix the sessionId that will be assigned to the session with this parameter.
        :param media_mode: ROUTED (default) or RELAYED
        :return: The created OpenViduSession instance.
        """
        # Prepare parameters
        if media_mode not in ['ROUTED', 'RELAYED', None]:
            raise ValueError(f"media_mode must be any of ROUTED or RELAYED, not {media_mode}")

        parameters = {"mediaMode": media_mode, "customSessionId": custom_session_id, "recordingMode": recording_mode}
        parameters = {k: v for k, v in parameters.items() if v is not None}

        # send request
        r = self._session.post('sessions', json=parameters)

        if r.status_code == 409:
            raise OpenViduSessionExistsError()
        elif r.status_code == 400:
            raise ValueError()

        r.raise_for_status()

        # As of OpenVidu 2.16.0 the server returns the created session object
        new_session = OpenViduSession(self._session, r.json())
        self._openvidu_sessions[new_session.id] = new_session

        return new_session

    @property
    def sessions(self) -> List[OpenViduSession]:
        """
        Get a list of currently active sessions to the server.

        :return: A list of OpenViduSession objects.
        """
        return [
            sess for sess in self._openvidu_sessions.values() if sess.is_valid
        ]

    @property
    def session_count(self) -> int:
        """
        Get the number of active sessions on the server.

        :return: The number of active sessions.
        """
        return len(self.sessions)

    def get_config(self) -> dict:
        """
        Get OpenVidu active configuration.

        Unlike session related calls. This call does not require prior calling of the fetch() method.
        Using this function will always result an API call to the backend.

        https://docs.openvidu.io/en/2.16.0/reference-docs/REST-API/#get-openviduapiconfig

        :return: The exact response from the server as a dict.
        """
        # Note: Since 2.16.0 This endpoint is moved from toplevel under /api
        # https://docs.openvidu.io/en/2.16.0/reference-docs/REST-API/#get-openviduapiconfig
        r = self._session.get('config')
        r.raise_for_status()

        return r.json()

    def start_recording(self, session_id: str, name: str = None, outputMode: str = None, hasAudo: bool = None, 
                        hasVideo: bool = None, recordingLayout: str = None, customLayout: str = None, 
                        resolution: str = None, frameRate: int = None, shmSize: int = None, 
                        ignoreFailedStreams: bool = None, mediaNode: dict = None):
        """
        Starts recording a session.
        :param session_id: The ID of the session to record.

        :param name: you want to give to the video file. You can access this same property in 
            openvidu-browser on recordingEvents. If no name is provided, 
            the video file will be named after id property of the recording.

        :param outputMode: ecord all streams in a single file in a grid layout or record each 
            stream in its own separate file.

        :param hasAudio:  (optional) whether to record audio or not. Default to true

        :param hasVideo: (optional)  whether to record video or not. Default to true

        :param recordingLayout: (optional String. Only applies if outputMode is set to COMPOSED or 
            COMPOSED_QUICK_START and hasVideo to true): the layout to be used in this recording.
            
        :param customLayout: (optional String. Only applies if recordingLayout is set to CUSTOM)
            a relative path indicating the custom recording layout to be used if more than one is available.

        :param resolution: (optional String. Only applies if outputMode is set to COMPOSED or 
            COMPOSED_QUICK_START and hasVideo to true) : the resolution of the recorded video file. 
                It is a string indicating the width and height in pixels like this: "1280x720"

        :param frameRate: (optional Number. Only applies if outputMode is set to COMPOSED or 
            COMPOSED_QUICK_START and hasVideo to true) : the frame rate of the recorded video file. 

        :param shmSize: (optional Number. Only applies if outputMode is set to COMPOSED or 
            COMPOSED_QUICK_START and hasVideo to true) : the amount of memory dedicated to the 
                recording module in charge of this specific recording, in bytes.

        :param ignoreFailedStreams: (optional Boolean. Only applies if outputMode is set to INDIVIDUAL) 
            whether to ignore failed streams or not when starting the recording.

        :param mediaNode: An object with the Media Node selector to force the Media Node 
            allocation of this recording.

        :returns OpenVidu Reording Object (https://docs.openvidu.io/en/stable/reference-docs/REST-API/#the-recording-object)
        
        """

        if session_id not in self._openvidu_sessions:
            raise OpenViduSessionDoesNotExistsError()

        session = self._openvidu_sessions[session_id]


        if not session.is_valid:
            raise OpenViduSessionDoesNotExistsError()

        print(name)
        
        parameters = {
            "session": session_id,
            "name": name,
            "outputMode": outputMode,
            "hasAudio": hasAudo,
            "hasVideo": hasVideo,
            "recordingLayout": recordingLayout,
            "customLayout": customLayout,
            "resolution": resolution,
            "frameRate": frameRate,
            "shmSize": shmSize,
            "ignoreFailedStreams": ignoreFailedStreams,
            "mediaNode": mediaNode

        }
        parameters = {k: v for k, v in parameters.items() if v is not None}
        print(parameters)
        r = self._session.post(f"recordings/start", json=parameters)
        print(r.content)

        return r.json()
    
    def stop_recording(self, recording_id):
        """
        Stops a recording

        :param recording_id: the id of the recording to stop recording

        :returns OpenVidu Reording Object (https://docs.openvidu.io/en/stable/reference-docs/REST-API/#the-recording-object)

        """

        r = self._session.post(f"recordings/stop/{recording_id}")

        return r.json()

    def get_recording(self, recording_id):
        """ Gets a specific recording 

        :param recording_id: the id of the recording to get.

        :returns OpenVidu Reording Object (https://docs.openvidu.io/en/stable/reference-docs/REST-API/#the-recording-object)        
        """

        r = self._session.get(f"recordings/{recording_id}")

        return r.json()

    def get_recordings(self):

        """ Gets all recordings 

        :returns a list of OpenVidu Reording Object (https://docs.openvidu.io/en/stable/reference-docs/REST-API/#the-recording-object)
        """

        r = self._session.get("recordings")
        return r.json()

    def delete_recording(self, recording_id):

        """Deletes a recording

        :param recording_id: the id of the recording to delete.

        :returns OpenVidu Reording Object (https://docs.openvidu.io/en/stable/reference-docs/REST-API/#the-recording-object)

        """
        r = self._session.post(f"recordings/delete/{recording_id}")
        return r.json()
